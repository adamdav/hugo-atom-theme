Responsive HTML5 website template for marketing mobile apps

Theme name:
=======================================================================
Atom

Theme version:
=======================================================================
v2.0

Release Date:
=======================================================================
07 Oct 2017

Author: 
=======================================================================
Xiaoying Riley at 3rd Wave Media

Contact:
=======================================================================
Web: http://themes.3rdwavemedia.com/
Email: themes@3rdwavemedia.com
Facebook: https://www.facebook.com/3rdwavethemes/
Twitter: 3rdwave_themes
